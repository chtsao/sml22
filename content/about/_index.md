---
title: "About"
date: 2022-02-11T07:45:39+08:00
draft: false
tags: []
categories: [admin]
---
![Robots](https://static2.therichestimages.com/wordpress/wp-content/uploads/2019/02/Robots.jpg)




### 課程資訊

* Curator: C. Andy Tsao.  Office: SE A411.  Tel: 3520, [Syllabus](https://sys.ndhu.edu.tw/AA/CLASS/TeacherSubj/prt_tplan.aspx?no=110241449)
* Lectures: 
  * 3D: Mon. **1310**-1400, Thr. **1410**-1500 @ AE A316.
  * Virtual: [GoogleClassRoom](https://classroom.google.com/c/MjI3ODA2MzQwODk4) 
* Office Hours:  Mon. 14:10-15:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎來，但若有導生來，則導生優先)
* Computing: R ([original](https://cran.r-project.org/), , [mirrors](https://cran.r-project.org/mirrors.html)); [Rstudio](https://www.rstudio.com/)  (IDE for R). You are welcome to use other languages or softwares nonetheless, e.g. Python, Matlab.
* Textbook: [Hastie, Tibshirani and Friedman (2009). The Elements of Statistical Learning: Data Mining, Inference and Prediction.](https://hastie.su.domains/ElemStatLearn/) 2nd Edition. (aka. ESLII)  Springer-Verlag. Or [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=844743&location=0&q=Elements+of+Statlstical+Machine+Learning&start=0&view=CONTENT)
* Prerequisites: Statistics, Linear Algebra. Knowledge about
regression or General/Generalized Linear Models will be helpful. 
