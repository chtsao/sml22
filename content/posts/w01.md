---
title: "Week 1a. SML Why, What and How"
date: 2022-02-11T07:27:39+08:00
draft: false
categories: [notes] 
tags: [why, gitlab, github, linux, data science]
---

![](https://www.magicalquote.com/wp-content/uploads/2018/07/Learning-does-not-consist-only-of-knowing-what-we-must-or-we-can-do-but-also-of-knowing-what-we-could-do-and-perhaps-should-not-do.jpg)

## Why

* ~~Metaverse~~ Dataverse: Data $\approx$ World (A variation on "In the beginning was the Word, and the Word was with God, and the Word was God." Opening of [The Name of the Rose](https://en.wikipedia.org/wiki/The_Name_of_the_Rose))
* 亂：期待與創造 新規則，新秩序，新系統的時代
* SWOT: 我該如何準備？

## What

* ~~BD, AI~~ Data Science: [OCEMIC](https://chtsao.gitlab.io/rgame21/posts/w01a/#why-what-and-how) (Obtain, Clean, Explore, Model, Interpret and Communicate)
* Machine Learning: Machine $\approx$ algorithm/APP, Learning $\approx$ Learn from data (data-based not rule-based)
  * Supervised Learning, Unsupervised Learning, Semi-supervised Learning
  * Find good estimate of $f$ for $y \approx f({\mathbf x})$ using training data $D_{train} = ( y_i, x_i )_{i=1}^n$
  * Applications: Search Engines, Recommendation systems, Pattern Recognition (More: [Moravec's paradox](https://en.wikipedia.org/wiki/Moravec%27s_paradox))
  

## Data Scientist

* Mindset: Passion+Curiosity; MIT Medialab's 4P (Project, Peers, Passion and Play)
* Skills
* 自品牌經營：作品、團隊、成長

<img src="https://assets.astronomer.io/website/img/blog/old/Picture1-5.png?profile=RESIZE_710x" style="zoom: 67%;" />

<img src="https://datacatchup.com/wp-content/uploads/2020/03/image-1.png" style="zoom:80%;" />
