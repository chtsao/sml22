---
title: Week 12.  ROC, AUC and pAUC
date: 2022-04-29T10:56:15+08:00
draft: false
categories: [notes]
tags: [ROC, AUC]
---
![roc](https://roc-streaming.org/logo.png)

### Converting scores to probability
* [Softmax transformation](https://deepai.org/machine-learning-glossary-and-terms/softmax-layer) aand [Sigmoid function](https://deepai.org/machine-learning-glossary-and-terms/sigmoid-function): configurations
* Probability integral formula
* Posterior probability: if possible, probably the best.

### Evaluating binary discrete classifiers
- Training/Testing Error: Convenient but rough
- Confusion matrix/Contingency Table: Better but restrictive to discrete classifiers (or probabilistic/score classifers of given thresholds)
- Over all threshod range $\leadsto$ ROC, etc
- ROC
	- Fawcett: [ROC101](http://binf.gmu.edu/mmasso/ROC101.pdf), [Intro to ROC curve](http://www.google.com.tw/url?sa=t&source=web&cd=4&ved=0CD0QFjAD&url=http%3A%2F%2Fciteseerx.ist.psu.edu%2Fviewdoc%2Fdownload%3Fdoi%3D10.1.1.98.4088%26rep%3Drep1%26type%3Dpdf&ei=bTAQTbGSGYKcvgOqp9DzDQ&usg=AFQjCNHO-_yjWAJrRVnJms7MbcqaJkd8eg&sig2=oi27hBSKISxEyhM2hpZXcg)
	- Packages and Codes of ROC: [Some R Packages for ROC Curves](https://www.r-bloggers.com/2019/02/some-r-packages-for-roc-curves/) @ [Rbloggers](https://www.r-bloggers.com/), [ROC Curves in Two Lines of R Code](https://blog.revolutionanalytics.com/2016/08/roc-curves-in-two-lines-of-code.html)
	- [Interpreting diagnostic tests](http://gim.unmc.edu/dxtests/Default.htm) ([ROC curve](http://gim.unmc.edu/dxtests/ROC1.htm))
	- Kaggle: [Home Credit Default Risk in which  ROC is the  evaluation criterion](https://www.kaggle.com/c/home-credit-default-risk/overview/evaluation)
	- My favoritest country :) [ROC](https://en.wikipedia.org/wiki/Taiwan)

