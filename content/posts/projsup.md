---
title: "Project Guides"
date: 2022-05-12T12:35:49+08:00
draft: false
tags: [Class Project]
---

<img src="https://i.pinimg.com/originals/70/61/56/7061562064400c46bc831b0e093d370d.jpg" style="zoom: 25%;" />

### Team Forming/Working/Rolling

### Proposal

* [Proposal@Berkeley](https://bcourses.berkeley.edu/courses/1377158/pages/final-project-proposal)
* [7 Steps to Writing the Perfect Project Proposal](https://www.fool.com/the-blueprint/project-proposal/)

### Written Report/Tech Writing

* [Technical Writing and Presentation @Uottawa](https://www.site.uottawa.ca/~rhabash/ELG2911TechnicalWritingandPresentation.pdf)

#### Technical Presentation

* [Slides Guideline for a Technical Presentation](https://www.cs.ucr.edu/~michalis/TECHWRITING/presentation-20.html)



### References and Links

* [研究生完全求生手冊：方法、秘訣、潛規則](https://www.books.com.tw/products/0010762863)

