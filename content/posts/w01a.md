---
title: "Week 1b. SML: A very short intro"
date: 2022-02-18T12:28:18+08:00
draft: false
categories: [notes] 
tags: [framework, regression, classification, loss]
---

<img src="https://s31531.pcdn.co/wp-content/uploads/2017/05/what-is-abstract-art-kandinsky.jpg.webp" style="zoom:80%;" />

## The Problem

Find **good** $f \in \mathcal{F}$ such that $Y \approx f({\mathbf X})$ given $(y_i, {\mathbf x}_i)_{i=1}^n \sim P_{Y, X}$
* $f$: Machine/Algorithm that yield a prediction/estimate of $y$ when an input ${\mathbf x}$ is given
* good:  In $Y \approx f({\mathbf X})$, the approximation is good  ($P_{Y, X}$). Usually, formalize to optimization of some loss $L$ or risk $R$ 
* $\mathcal{F}$: the class of all candidate machines/functions  compared or chosen from. 

## RTDM but ...

* Computation facility is cheep and everywhere **BUT** we, human, need to know more about 
  * how to direct them to do what we want and 
  * how to correctly understand and interpret the output. 
  * Example, categorical-continuous encoding/variables; regression output summary.
* Better start with a basic/foundational understanding of the algorithms. 
  * Prioritize the parameters
  * Know how and why the algorithm works and do work. Know its strength and weakness. 
  * Have more control on the package/codes: Break and fix $\leadsto$ Better understanding. Modification/Refinement. 

## Regression

