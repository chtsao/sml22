---
title: "Week 8. Elementary Machines"
date: 2022-04-06T15:16:26+08:00
draft: false
categories: [notes, adm]
tags: [classification, classifier, binary, multiclass]
---
![Foundation](https://evolllution.com/wp-content/uploads/2018/07/Zanville-Aug-1-2018-Sized.jpeg)

### CovirtualClassRoom

* 配合本校[防疫措施](https://rb005.ndhu.edu.tw/p/404-1005-194437-1.php?Lang=zh-tw) 4/6-4/16 採線上上課
* 本課程上課時間不變仍為 Thr 1310-1500; Mon 1310-1400，但以線上方式實施。
* GoogleMeet 連結: https://meet.google.com/bps-pjyr-uhr

### Elementary
Elementary 在這裡是指基本而簡單，更進一步說是**基礎**或**核心**。這些方法本身不見得是個好的分類器，但經過適當的調校與整合，他們可以疊積成有效而強大的分類器。對這些 elementary classifiers 的深度了解，將可讓你在使用他們延伸的分類器、建構組合新的分類器或是使用相關軟體套件的參數調整時，更能得心應手，知道自己在做些什麼。

#### Classifying classifiers: 

You may use Naive regression vs. KNN for comparison and contrast. 
1. Linear vs. Nonlinear
2. Model-based vs. Distance-based
3. Global vs. Local
4. "\\( \beta\\)"-centered vs Y-centered

##### Encoding: indicator matrix: Y and X

1. One-hot encoding
2. one-vs-one, one-vs-all
3. partition-based encoding
4. categorization of continuous variables

##### Logistic Regression, FDA (LDA and QDA)

### Slides
這些是到目前為止上課所使用的投影片，與實際上課內容與重點稍有出入。但還是有些提示重點的參考價值: [Set01](http://faculty.ndhu.edu.tw/~chtsao/ftp/sml/smlw01.pdf), [Set02](http://faculty.ndhu.edu.tw/~chtsao/ftp/sml/smlw02n.pdf), [Set03](http://faculty.ndhu.edu.tw/~chtsao/ftp/sml/smlw03n.pdf).

